## 获取借贷记录
```request
{
　　"accesskey":"your accesskey",
　　"channel":"getLoanRecords",
　　"loanId":58,
　　"marketName":"btsqc",
　　"pageIndex":1,
　　"pageSize":10,
　　"sign":签名,
　　"status":""
}
```
```response
{
　　"success":true,
　　"code":1000,
　　"data":[
　　　　{
　　　　　　"createTime":1521789778000,
　　　　　　"statusShow":"还款中",
　　　　　　"freezId":"0",
　　　　　　"tstatus":0,
　　　　　　"withoutLxAmount":"0",
　　　　　　"investMark":false,
　　　　　　"withoutLxDays":0,
　　　　　　"hasRepay":"0",
　　　　　　"amount":"600",
　　　　　　"id":59,
　　　　　　"fwfScale":"0.2",
　　　　　　"rate":"0.0015",
　　　　　　"marketName":"btsqc",
　　　　　　"hasLx":"0",
　　　　　　"isIn":false,
　　　　　　"balanceAmount":"0",
　　　　　　"fundType":15,
　　　　　　"outUserId":110652,
　　　　　　"inUserId":110652,
　　　　　　"repayDate":1524381778000,
　　　　　　"zheLx":"0",
　　　　　　"outUserFees":"",
　　　　　　"dikouLx":"0",
　　　　　　"sourceType":8,
　　　　　　"coinName":"QC",
　　　　　　"reward":"0",
　　　　　　"status":1,
　　　　　　"arrearsLx":"0.9",
　　　　　　"statusColor":"orange",
　　　　　　"balanceWithoutLxDays":0,
　　　　　　"riskManage":1,
　　　　　　"rateAddVal":"0",
　　　　　　"outUserName":"13800138000",
　　　　　　"inUserName":"13800138000",
　　　　　　"inUserLock":false,
　　　　　　"rateForm":1,
　　　　　　"loanId":58,
　　　　　　"nextRepayDate":1521876178000
　　　　}
　　],
　　"channel":"getLoanRecords",
　　"message":"操作成功",
　　"no":"0"
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
accesskey | String | accesskey
channel | String | getLoanRecords
event | String | 直接赋值addChannel
loanId | Long | 理财id（借出就传 借入不用传）
marketName | String | 市场
status | int | 状态(1还款中 2已还款 3 需要平仓 4 平仓还款 5自动还款中 6自动还款)
pageIndex | int | 当前页数
pageSize | int | 每页数量
no | String | 请求的唯一标识，用于在返回内容时区分
sign |String | 签名

```resdata
success : 是否成功
code : 返回代码
message : 提示信息
channel : 请求的频道
no : 请求的唯一标识，用于在返回内容时区分
data :
    id : 借贷记录ID
    loanId : 理财ID
    inUserId : 借入者ID
    inUserName : 借入者用户名
    outUserId : 借出者ID
    outUserName : 借出者用户名
    fundType : 币种类型
    amount : 借入金额
    status : 状态 1还款中 2已还款 3需要平仓 4 平仓还款 5自动还款
    createTime : 借贷成功时间
    reward : 奖励金额
    balanceAmount : 投标后借款剩余金额
    rate : 利率
    hasRepay :已还本金金额
    hasLx : 已还利息
    dikouLx : 已抵扣利息
    zheLx : 折算线上价
    arrearsLx : 拖欠利息
    nextRepayDate : 下次还款时间
    riskManage : 风险控制
    inUserLock : 借入者是否被锁定
    withoutLxAmount : 免息额度
    withoutLxDays : 免息天数
    balanceWithoutLxDays : 剩余的免息天数
    rateForm : 利率形式
    rateAddVal : 增长幅度
    repayDate : 还款时间
    marketName : 市场
    fwfScale : 服务费费率
    investMark : 自动续借
    sourceType : 来源类型： 8”网页”，5”手机APP”，6”接口API”
```

```java
public void getLoanRecords() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String secret = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String params = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"getLoanRecords","event":"addChannel","loanId":"58","marketName":"btsqc","no":"test001","pageIndex":"1","pageSize":"10"}";
	//sign通过HmacMD5加密得到:a728271842900000f82a046d06f5ceee
	String sign = EncryDigestUtil.hmacSign(params, secret);
	//最终发送到服务器参数json请求
	String json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"getLoanRecords","event":"addChannel","loanId":"58","marketName":"btsqc","no":"test001","pageIndex":"1","pageSize":"10","sign":"a728271842900000f82a046d06f5ceee"}";
	ws.sendText(json);
}
```

```python
import hashlib
import zlib
import json
from time import sleep
from threading import Thread

import websocket    
import urllib2, hashlib,struct,sha,time


zb_usd_url = "wss://api.zb.cn:9999/websocket"

class ZB_Sub_Spot_Api(object):
    """基于Websocket的API对象"""
    def __init__(self):
        """Constructor"""
        self.apiKey = 'ce2a18e0-dshs-4c44-4515-9aca67dd706e'        
        self.secretKey = 'c11a122s-dshs-shsa-4515-954a67dd706e'     

        self.ws_sub_spot = None          # websocket应用对象  现货对象

    #----------------------------------------------------------------------
    def reconnect(self):
        """重新连接"""
        # 首先关闭之前的连接
        self.close()
        
        # 再执行重连任务
        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                         on_message=self.onMessage,
                                         on_error=self.onError,
                                         on_close=self.onClose,
                                         on_open=self.onOpen)        
    
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()
    
    #----------------------------------------------------------------------
    def connect_Subpot(self, apiKey , secretKey , trace = False):
        self.host = zb_usd_url
        self.apiKey = apiKey
        self.secretKey = secretKey

        websocket.enableTrace(trace)

        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                             on_message=self.onMessage,
                                             on_error=self.onError,
                                             on_close=self.onClose,
                                             on_open=self.onOpen)        
            
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()

    #----------------------------------------------------------------------
    def readData(self, evt):
        """解压缩推送收到的数据"""
        # # 创建解压器
        # decompress = zlib.decompressobj(-zlib.MAX_WBITS)
        
        # # 将原始数据解压成字符串
        # inflated = decompress.decompress(evt) + decompress.flush()
        
        # 通过json解析字符串
        data = json.loads(evt)
        
        return data

    #----------------------------------------------------------------------
    def close(self):
        """关闭接口"""
        if self.thread and self.thread.isAlive():
            self.ws_sub_spot.close()
            self.thread.join()

    #----------------------------------------------------------------------
    def onMessage(self, ws, evt):
        """信息推送""" 
        print evt
        
    #----------------------------------------------------------------------
    def onError(self, ws, evt):
        """错误推送"""
        print 'onError'
        print evt
        
    #----------------------------------------------------------------------
    def onClose(self, ws):
        """接口断开"""
        print 'onClose'
        
    #----------------------------------------------------------------------
    def onOpen(self, ws):
        """接口打开"""
        print 'onOpen'
    
    #----------------------------------------------------------------------
    def __fill(self, value, lenght, fillByte):
        if len(value) >= lenght:
            return value
        else:
            fillSize = lenght - len(value)
        return value + chr(fillByte) * fillSize
    #----------------------------------------------------------------------
    def __doXOr(self, s, value):
        slist = list(s)
        for index in xrange(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)
    #----------------------------------------------------------------------
    def __hmacSign(self, aValue, aKey):
        keyb   = struct.pack("%ds" % len(aKey), aKey)
        value  = struct.pack("%ds" % len(aValue), aValue)
        k_ipad = self.__doXOr(keyb, 0x36)
        k_opad = self.__doXOr(keyb, 0x5c)
        k_ipad = self.__fill(k_ipad, 64, 54)
        k_opad = self.__fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad)
        m.update(value)
        dg = m.digest()
        
        m = hashlib.md5()
        m.update(k_opad)
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def __digest(self, aValue):
        value  = struct.pack("%ds" % len(aValue), aValue)
        h = sha.new()
        h.update(value)
        dg = h.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def generateSign(self, params):
        #参数按照ASCII值排序: {"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"getLoanRecords","event":"addChannel","loanId":"58","marketName":"btsqc","no":"test001","pageIndex":"1","pageSize":"10"}
        #secretKey 加密后:86429c69799d3d6ac5da5c2c514baa874d75a4ba
        SHA_secret = self.__digest(self.secretKey)
        #计算出sign: a728271842900000f82a046d06f5ceee
        return self.__hmacSign( paramsStr, SHA_secret)

    #----------------------------------------------------------------------
    def getLoanRecords(self, symbol_pair, type_, price, amount):
        json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"getLoanRecords","event":"addChannel","loanId":"58","marketName":"btsqc","no":"test001","pageIndex":"1","pageSize":"10","sign":"a728271842900000f82a046d06f5ceee"}";
        try:
            self.ws_sub_spot.send(json)
        except websocket.WebSocketConnectionClosedException:
            pass 
```