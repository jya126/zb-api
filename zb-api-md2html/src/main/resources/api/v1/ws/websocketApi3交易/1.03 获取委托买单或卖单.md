## 获取委托买单或卖单
```request
{
    "accesskey": your key,
    "channel": "ltcbtc_getorder",
    "event": "addChannel",
    "id": 20160902387645980,
    "no": "14728149875431230591268",
    "sign": 签名,
}
```
```response
{
    "success": true,
    "code": 1000,
    "data": {
        "currency": "ltc_btc",
        "fees": 0,
        "id": "20160902387645980",
        "price": 100,
        "status": 0,
        "total_amount": 0.01,
        "trade_amount": 0,
    	"trade_price" : 6000,
        "trade_date": 1472814905567,
        "trade_money": 0,
        "type": 1
    },
    "channel": "ltcbtc_getorder",
    "message": "操作成功。",
    "no": "14728149875431230591268"
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
accesskey | String | accesskey
channel | String | 交易币种_购买币种_getorder
event | String | 直接赋值addChannel
id | long | 委托挂单号
no | String | 请求的唯一标识，用于在返回内容时区分
sign | String | 请求加密签名串

```resdata
id : 委托挂单号
type : 挂单类型 1/0[buy/sell]
type : 挂单类型 1/0[buy/sell]
currency : 交易类型
trade_amount : 已成交数量
trade_money : 已成交总金额
trade_price : 成交均价
total_amount : 挂单总数量
trade_date : 委托时间
fees : 交易手续费,卖单的话,显示的是收入货币;买单的话,显示的是买入货币
status : 挂单状态（1：取消，2：交易完成，0/3：待成交/待成交未交易部份）
code : 返回代码
message : 提示信息
channel : 请求的频道
no : 请求的唯一标识，用于在返回内容时区分
```

```java
public void usdtqc_getorder() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String secret = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String params = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"usdtqc_getorder","event":"addChannel","id":"20180522105585216","no":"test001"}";
	//sign通过HmacMD5加密得到:aa67749256db23191458fea8970f1b6e
	String sign = EncryDigestUtil.hmacSign(params, secret);
	//最终发送到服务器参数json请求
	String json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"usdtqc_getorder","event":"addChannel","id":"20180522105585216","no":"test001","sign":"aa67749256db23191458fea8970f1b6e"}";
	ws.sendText(json);
}
```

```python
import hashlib
import zlib
import json
from time import sleep
from threading import Thread

import websocket    
import urllib2, hashlib,struct,sha,time


zb_usd_url = "wss://api.zb.cn:9999/websocket"

class ZB_Sub_Spot_Api(object):
    """基于Websocket的API对象"""
    def __init__(self):
        """Constructor"""
        self.apiKey = 'ce2a18e0-dshs-4c44-4515-9aca67dd706e'        
        self.secretKey = 'c11a122s-dshs-shsa-4515-954a67dd706e'     

        self.ws_sub_spot = None          # websocket应用对象  现货对象

    #----------------------------------------------------------------------
    def reconnect(self):
        """重新连接"""
        # 首先关闭之前的连接
        self.close()
        
        # 再执行重连任务
        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                         on_message=self.onMessage,
                                         on_error=self.onError,
                                         on_close=self.onClose,
                                         on_open=self.onOpen)        
    
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()
    
    #----------------------------------------------------------------------
    def connect_Subpot(self, apiKey , secretKey , trace = False):
        self.host = zb_usd_url
        self.apiKey = apiKey
        self.secretKey = secretKey

        websocket.enableTrace(trace)

        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                             on_message=self.onMessage,
                                             on_error=self.onError,
                                             on_close=self.onClose,
                                             on_open=self.onOpen)        
            
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()

    #----------------------------------------------------------------------
    def readData(self, evt):
        """解压缩推送收到的数据"""
        # # 创建解压器
        # decompress = zlib.decompressobj(-zlib.MAX_WBITS)
        
        # # 将原始数据解压成字符串
        # inflated = decompress.decompress(evt) + decompress.flush()
        
        # 通过json解析字符串
        data = json.loads(evt)
        
        return data

    #----------------------------------------------------------------------
    def close(self):
        """关闭接口"""
        if self.thread and self.thread.isAlive():
            self.ws_sub_spot.close()
            self.thread.join()

    #----------------------------------------------------------------------
    def onMessage(self, ws, evt):
        """信息推送""" 
        print evt
        
    #----------------------------------------------------------------------
    def onError(self, ws, evt):
        """错误推送"""
        print 'onError'
        print evt
        
    #----------------------------------------------------------------------
    def onClose(self, ws):
        """接口断开"""
        print 'onClose'
        
    #----------------------------------------------------------------------
    def onOpen(self, ws):
        """接口打开"""
        print 'onOpen'
    
    #----------------------------------------------------------------------
    def __fill(self, value, lenght, fillByte):
        if len(value) >= lenght:
            return value
        else:
            fillSize = lenght - len(value)
        return value + chr(fillByte) * fillSize
    #----------------------------------------------------------------------
    def __doXOr(self, s, value):
        slist = list(s)
        for index in xrange(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)
    #----------------------------------------------------------------------
    def __hmacSign(self, aValue, aKey):
        keyb   = struct.pack("%ds" % len(aKey), aKey)
        value  = struct.pack("%ds" % len(aValue), aValue)
        k_ipad = self.__doXOr(keyb, 0x36)
        k_opad = self.__doXOr(keyb, 0x5c)
        k_ipad = self.__fill(k_ipad, 64, 54)
        k_opad = self.__fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad)
        m.update(value)
        dg = m.digest()
        
        m = hashlib.md5()
        m.update(k_opad)
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def __digest(self, aValue):
        value  = struct.pack("%ds" % len(aValue), aValue)
        h = sha.new()
        h.update(value)
        dg = h.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def generateSign(self, params):
        #参数按照ASCII值排序: {"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"usdtqc_getorder","event":"addChannel","id":"20180522105585216","no":"test001"}
        #secretKey 加密后:86429c69799d3d6ac5da5c2c514baa874d75a4ba
        SHA_secret = self.__digest(self.secretKey)
        #计算出sign: aa67749256db23191458fea8970f1b6e
        return self.__hmacSign( paramsStr, SHA_secret)

    #----------------------------------------------------------------------
    def usdtqc_getorder(self, symbol_pair, type_, price, amount):
        json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","channel":"usdtqc_getorder","event":"addChannel","id":"20180522105585216","no":"test001","sign":"aa67749256db23191458fea8970f1b6e"}";
        try:
            self.ws_sub_spot.send(json)
        except websocket.WebSocketConnectionClosedException:
            pass 
```