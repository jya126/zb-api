## 获取allTicker数据
```request
GET http://api.zb.cn/data/v1/allTicker
```
```response
{
    "hpybtc":{
		"vol":"19800.6",
		"last":"0.00000237",
		"sell":"0.000002366",
		"buy":"0.00000236",
		"high":"0.0000026552",
		"low":"0.00000229"
	},
	"tvqc":{
		"vol":"2201510.1",
		......
}
```

**请求参数**

参数名称 | 类型 | 取值范围
---|---|---


```resdata
high : 最高价
low : 最低价
buy : 买一价
sell : 卖一价
last : 最新成交价
vol : 成交量(最近的24小时)
```


```java
public void getAllTicker() {
	//得到返回结果
	String returnJson = HttpRequest.get("http://api.zb.cn/data/v1/allTicker").send().bodyText();
}
```

```python
def get(self, url):
    while True:
        try:
            r = requests.get(url)
        except Exception:
            time.sleep(0.5)
            continue
        if r.status_code != 200:
            time.sleep(0.5)
            continue
        r_info = r.json()
        r.close()
        return r_info
        
def getAllTicker(self, market):
    url = 'http://api.zb.cn/data/v1/allTicker'
    return self.get(url)
```