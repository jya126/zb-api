## 获取市场深度
```request
GET http://api.zb.cn/data/v1/depth?market=btc_usdt&size=3
```
```response
{
    "asks": [
        [
            83.28,
            11.8
        ]...
    ],
    "bids": [
        [
            81.91,
            3.65
        ]...
    ],
    "timestamp" : 时间戳
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
market | String | 例：btc_qc
size | int | 档位1-50，如果有合并深度，只能返回5档深度
merge | float | 默认深度

```resdata
asks : 卖方深度
bids : 买方深度
timestamp : 此次深度的产生时间戳
```

```java
public void getDepth() {
	//得到返回结果
	String returnJson = HttpRequest.get("http://api.zb.cn/data/v1/depth?market=btc_usdt&size=3").send().bodyText();
}
```

```python
def get(self, url):
    while True:
        try:
            r = requests.get(url)
        except Exception:
            time.sleep(0.5)
            continue
        if r.status_code != 200:
            time.sleep(0.5)
            continue
        r_info = r.json()
        r.close()
        return r_info
        
def getDepth(self, market):
    url = 'http://api.zb.cn/data/v1/depth?market=btc_usdt&size=3'
    return self.get(url)
```