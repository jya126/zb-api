package zb.md2html.gen;

import java.io.IOException;

import jodd.io.findfile.FindFile;
import kits.my.BufferKit;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import zb.md2html.api.IHtml;
import zb.md2html.kit.MdKit;

/**
 * api 公共信息
 * 
 * @author Administrator
 *
 */
@Slf4j
@RequiredArgsConstructor
public class Info implements IHtml{
	private @NonNull String fileInfos;

	@Override
	public String getApiContent() {
		val sb = new BufferKit();
		FindFile.create().searchPath(fileInfos).findAll().forEach(file -> {
			log.debug("公共信息:" + file);
			try {
				sb.append(MdKit.readMd2Str(file));
			} catch (IOException e) {
				log.error("公共信息异常",e);
			}
		});
		return sb.toString();
	}
}
